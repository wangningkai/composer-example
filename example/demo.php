<?php

require __DIR__ . '/../vendor/autoload.php';

// 调用类方法
$class = new App\ComposerExample(10);
echo $class->getRandStr(); //nn3waKUwNs

echo PHP_EOL;

// 直接通过助手函数
echo rand_str(); //cmKs6M
