<?php

namespace App;

class ComposerExample
{
    /**
     * 限制长度
     *
     * @var int
     */
    public $length = 6;
    
    /**
     * @param int $length
     */
    public function __construct($length)
    {
        $this->length = $length;
    }
    
    /**
     * 获取随机字符串
     *
     * @return string
     */
    public function getRandStr()
    {
        $length = $this->length;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
        for ($i = 0; $i < $length; $i++) { 
            $randomString .= $characters[rand(0, strlen($characters) - 1)]; 
        } 
        return $randomString; 
    }
}
